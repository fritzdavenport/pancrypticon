#!/bin/bash

# start cron
/etc/init.d/cron start

/usr/local/sbin/certbot.sh && \
/usr/local/sbin/config.sh && \
/usr/sbin/nginx -g 'daemon off;'
