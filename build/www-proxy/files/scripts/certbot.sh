#!/bin/bash

# use fake cert for testing
if [ $TESTING ]; then
    /bin/mkdir -p /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME && \
    /bin/cp /etc/ssl/certs/ssl-cert-snakeoil.pem \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/fullchain.pem && \
    /bin/cp /etc/ssl/private/ssl-cert-snakeoil.key \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/privkey.pem

## generate and install a letsencrypt cert
elif [ ! -f /etc/letsencrypt/.installed ]; then
    /usr/bin/certbot \
    certonly \
    --standalone \
    --email $ADMIN_EMAIL \
    --agree-tos \
    --hsts \
    --rsa-key-size 4096 \
    --expand \
    --noninteractive \
    --domain $NEXTCLOUD_DOMAIN_NAME \
    --domain $OFFICE_DOMAIN_NAME && \
    /bin/echo `date +%s` > /etc/letsencrypt/.installed
else
    /usr/bin/certbot renew --non-interactive
fi

# generate new Diffie-Helmann parameters
if [ ! -f /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem ]; then
    /usr/sbin/rngd -r /dev/urandom
    /usr/bin/openssl dhparam -out \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem 2048 && \
    chmod 600 /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem && \
    chown www-data /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem
fi
