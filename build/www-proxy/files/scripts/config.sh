#!/bin/bash
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/nginx/sites-enabled/default && \
/bin/sed -i "s|OFFICE_DOMAIN_NAME|$OFFICE_DOMAIN_NAME|g" \
    /etc/nginx/sites-enabled/default && \

if [ $TESTING ]; then
    /bin/sed -i "s|SSLCertificateChainFile|#SSLCertificateChainFile|g" \
        /etc/nginx/sites-enabled/default
else
    /bin/sed -i "s|#SSLCertificateChainFile|SSLCertificateChainFile|g" \
        /etc/nginx/sites-enabled/default
fi
