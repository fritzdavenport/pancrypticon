#!/usr/bin/python

import inotify.adapters, os, re, subprocess, time, thread, Queue
from collections import deque

class Watcher(object):
    """
    Sets INOTIFY watches on a directory and all of its children looking for
    changes to files and enqueueing them
    """
    def __init__(self, root='.', queue=None):
        self.queue = queue
        self.root = root

    def start(self):
        """
        Looks for CLOSE_WRITE, MODIFY, and MOVED_TO events. CREATEs are skipped
        because Nextcloud first writes to a temporary location and then performs
        a move.
        """
        os.chdir(self.root)
        i = inotify.adapters.InotifyTree(self.root)

        for event in i.event_gen():
            if event is not None:
                (header, type_names, watch_path, filename) = event
                # capture inotify events of the listed types
                watch_types = set(type_names).intersection(
                    ['IN_CLOSE_WRITE', 'IN_MODIFY', 'IN_MOVED_TO']
                )
                if len(watch_types):
                    if re.search('\.(part|VIRUS_DETECTED)$', filename):
                        continue
                    self.queue.put(
                        {
                            'filename'  : filename,
                            'path'      : watch_path,
                        }
                    )

class Scanner(object):
    """
    Scans enqueued files with ClamAV, then passes any hits off to an outbound
    queue
    """
    def __init__(self, in_queue=None, out_queue=None, root='.'):
        self.in_q = in_queue
        self.out_q = out_queue
        self.root = root

    def start(self):
        os.chdir(self.root)
        while True:
            try:
                data = self.in_q.get_nowait()
                if data is not None:
                    full_name = "%s/%s" % (data['path'], data['filename'])
                    # logger("Scanning %s" % full_name)
                    p = subprocess.Popen(
                        [
                            '/usr/bin/clamdscan',
                            '--fdpass',
                            '--infected',
                            '--no-summary',
                            full_name,
                        ],
                        stdout=subprocess.PIPE
                    )

                    for line in iter(p.stdout.readline, ''):
                        """
                        The --no-summary and --infected flags to clamdscan mean
                        only positive matches are printed, so we can treat any
                        output as a result
                        """
                        local_path = re.sub(
                            '^%s\/' % re.escape(self.root),
                            '',
                            data['path'],
                        )
                        infected = '%s/%s' % (local_path, data['filename'])
                        logger(line.rstrip())
                        #  rename the file with a .VIRUS_DETECTED extension
                        subprocess.Popen(
                            [
                                '/bin/mv',
                                infected,
                                '%s.VIRUS_DETECTED' % infected,
                            ]
                        )
                        self.out_q.put(local_path)
            except Queue.Empty:
                time.sleep(0.01)

class NextcloudUpdater(object):
    """
    Scans the locations of moved/renamed files to update Nextcloud's index
    """
    def __init__(self, queue=None):
        self.queue = queue
        self.locked = False

    def dedupe_queue(self):
        """
        Since Nextcloud's file:scan operation is unavoidably recursive this can
        be a very expensive operation just to catch one file in the root path,
        so it's important to make sure we minimize unnecessary repeat scans,
        hence we dedupe the queue before every 'get'
        """
        self.locked = True
        deduped = []
        try:
            while not self.queue.empty():
                item = self.queue.get_nowait()
                if item not in deduped:
                    deduped.append(item)
            for item in deduped:
                self.queue.put(item)
        except Queue.Empty:
            pass
        self.locked = False

    def start(self):
        while True:
            try:
                if self.queue.empty():
                    time.sleep(0.01)
                else:
                    self.dedupe_queue()
                    # check the lock in case multiple consumers are pulling from
                    # the queue
                    if self.locked:
                        time.sleep(0.01)
                    else:
                        path = self.queue.get_nowait()
                        if path is not None:
                            subprocess.Popen(
                                [
                                    '/usr/bin/php',
                                    '/opt/nextcloud/occ',
                                    'files:scan',
                                    '--path=%s' % path,
                                    '--no-interaction',
                                ]
                            )
            except Queue.Empty:
                time.sleep(0.01)

def logger(msg):
    print ('[%s] %s' % (time.ctime(), msg))

def main():
    data_dir = '/opt/nextcloud-data'
    pending_q = Queue.Queue()
    rescan_q = Queue.Queue()
    watcher = Watcher(root=data_dir, queue=pending_q)
    scanner = Scanner(in_queue=pending_q, out_queue=rescan_q, root=data_dir)
    updater = NextcloudUpdater(queue=rescan_q)

    logger("Setting up watchers on %s..." % data_dir)

    thread.start_new_thread(watcher.start, ())
    thread.start_new_thread(scanner.start, ())
    thread.start_new_thread(updater.start, ())

    while True:
        time.sleep(0.1)

main()
