#!/bin/bash

# set user credentials for ejabberd database
/bin/sed -i "s|MYSQL_USER|$MYSQL_USER|g" /docker-entrypoint-initdb.d/01-ejabberd.sql && \
/bin/sed -i "s|MYSQL_PASSWORD|$MYSQL_PASSWORD|g" /docker-entrypoint-initdb.d/01-ejabberd.sql
