#!/bin/bash

/usr/local/sbin/install.sh && \

# Do not proceed until the database is available
while [ `/bin/nc -z mariadb 3306` ]; do
    /bin/echo "*** Waiting for database container to initialize..."
    /bin/sleep 3
done && \

/etc/init.d/ejabberd start && \
/usr/bin/tail -f /var/log/ejabberd/*
