#!/bin/bash

XMPP_CLOUD_AUTH_VERSION='0.2.1'

if [ ! $TESTING ]; then
    # create combined cert
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/privkey.pem > \
        /etc/ejabberd/ejabberd.pem && \
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/cert.pem >> \
        /etc/ejabberd/ejabberd.pem && \
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/chain.pem >> \
        /etc/ejabberd/ejabberd.pem
fi

# substitute values from settings.env
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/ejabberd/ejabberd.yml \
    /etc/external_cloud.conf && \
/bin/sed -i "s|OJSXC_TOKEN|$OJSXC_TOKEN|g" /etc/external_cloud.conf

# set database credentials, etc
/bin/sed -i "s|MYSQL_USER|$MYSQL_USER|g" /etc/ejabberd/ejabberd.yml && \
/bin/sed -i "s|MYSQL_PASSWORD|$MYSQL_PASSWORD|g" /etc/ejabberd/ejabberd.yml && \
/bin/sed -i "s|LANGUAGE|$LANGUAGE|g" /etc/ejabberd/ejabberd.yml

if [ ! -e /opt/xmpp-cloud-auth ]; then
    cd /opt && \
    /usr/bin/wget --quiet --show-progress --progress=bar \
        https://download.gibberfish.org/pancrypticon/ejabberd/xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz && \
    /usr/bin/wget --quiet --show-progress --progress=bar \
        https://download.gibberfish.org/pancrypticon/ejabberd/xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz.sha256 && \
    /usr/bin/wget --quiet --show-progress --progress=bar \
        https://download.gibberfish.org/pancrypticon/ejabberd/xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz.sha256.asc && \

    # verify the checksum or die
    if /usr/bin/sha256sum --strict -c \
        xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz.sha256; then
        /bin/echo \
            "  * [OK] Verfied sha256 checksum for xmpp-cloud-auth-${XMPP_CLOUD_AUTH_VERSION}.tar.gz"
    else
        /bin/echo \
            "  * [FATAL] xmpp-cloud-auth-${XMPP_CLOUD_AUTH_VERSION}.tar.gz CHECKSUM DOES NOT MATCH!"
        exit 255
    fi

    # verify the signature or die
    if /usr/bin/gpg --verify \
        xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz.sha256.asc; then
        /bin/echo \
            "  * [OK] Verfified GPG signature for xmpp-cloud-auth-${XMPP_CLOUD_AUTH_VERSION}.tar.gz.sha256"
    else
        /bin/echo \
            "  * [FATAL] xmpp-cloud-auth-${XMPP_CLOUD_AUTH_VERSION}.tar.gz.sha256 HAS AN INVALID SIGNATURE!"
        exit 255
    fi

    /bin/tar -xf xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION.tar.gz && \
    /bin/ln -s xmpp-cloud-auth-$XMPP_CLOUD_AUTH_VERSION xmpp-cloud-auth && \
    /bin/chmod u+x /opt/xmpp-cloud-auth/external_cloud.py && \
    /bin/chmod u+x /opt/xmpp-cloud-auth/external_cloud.sh && \
    /bin/chmod 640 /etc/external_cloud.conf && \
    /usr/bin/pip install -q ConfigArgParse && \
    /bin/chown ejabberd.ejabberd -R \
        /opt/xmpp-cloud-auth* \
        /etc/external_cloud.conf \
        /etc/ejabberd && \
    # fix messed up closing quotes
    /bin/sed -i "s|libraries\.\")$|libraries.')|" \
        /opt/xmpp-cloud-auth/external_cloud.py
fi
