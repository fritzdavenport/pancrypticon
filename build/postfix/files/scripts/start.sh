#!/bin/bash

/bin/echo $NEXTCLOUD_DOMAIN_NAME > /etc/mailname

/etc/init.d/rsyslog start

/usr/sbin/postfix start && /bin/sleep 5 && /usr/bin/tail -f /var/log/mail.*
