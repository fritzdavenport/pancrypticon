#!/bin/bash

# make sure all of the supported apps have the supported version installed

CALENDAR_VERSION="1.5.3"
CIRCLES_VERSION="0.12.4"
CONTACTS_VERSION="1.5.3"
DASHBOARD_VERSION="4.0.5"
FILES_ACCESSCONTROL_VERSION="1.2.4"
FILES_AUTOMATEDTAGGING_VERSION="1.2.2"
FILES_RETENTION_VERSION="1.1.2"
GROUPFOLDERS_VERSION="1.1.0"
OJSXC_VERSION="3.3.0"
RICHDOCUMENTS_VERSION="1.12.34"
SPREED_VERSION="2.0.1"
TASKS_VERSION="0.9.5"
TWOFACTOR_TOTP_VERSION="1.3.1"

function update_app {
    app=$1
    version=$2
    file="${app}-${version}.tar.gz"
    chksum="${file}.sha256"
    sig="${file}.sha256.asc"

    /bin/echo "*** Checking ${app} app..."
    if [ ! -d apps/$app ]; then
        /usr/bin/wget --quiet --show-progress --progress=bar \
            https://download.gibberfish.org/pancrypticon/nextcloud/$file && \
        /usr/bin/wget --quiet --show-progress --progress=bar \
            https://download.gibberfish.org/pancrypticon/nextcloud/$chksum && \
        /usr/bin/wget --quiet --show-progress --progress=bar \
            https://download.gibberfish.org/pancrypticon/nextcloud/$sig

    # # if the app is missing or the info.xml shows the wrong version, this will
    # # return false
    # if ! /bin/grep --silent $version apps/$app/appinfo/info.xml > /dev/null 2&>1; then
    #     /bin/echo "  * Updating ${app}..."
    #     # download the tarball, it's sha256 checksum file and GPG sig
    #     /usr/bin/wget --quiet --show-progress --progress=bar \
    #         https://download.gibberfish.org/pancrypticon/nextcloud/$file && \
    #     /usr/bin/wget --quiet --show-progress --progress=bar \
    #         https://download.gibberfish.org/pancrypticon/nextcloud/$chksum && \
    #     /usr/bin/wget --quiet --show-progress --progress=bar \
    #         https://download.gibberfish.org/pancrypticon/nextcloud/$sig
    #
        # verify the checksum or die

        if /usr/bin/sha256sum --strict -c $chksum; then
            /bin/echo "  * [OK] Verfied sha256 checksum for $file"
        else
            /bin/echo "  * [FATAL] $file CHECKSUM DOES NOT MATCH!"
            exit 255
        fi

        # verify the signature or die
        if /usr/bin/gpg --verify $sig $chksum; then
            /bin/echo "  * [OK] Verfified GPG signature for ${chksum}"
        else
            /bin/echo "  * [FATAL] ${chksum} HAS AN INVALID SIGNATURE!"
            exit 255
        fi

        # unpack the tarball and enable the app
        /bin/tar -xf $file -C apps/ && \
        /bin/chown -R www-data:www-data apps/$app && \
        /usr/bin/sudo -u www-data php occ app:enable $app
        # clean up
        /bin/rm $file $chksum $sig
    fi
}

cd /opt/nextcloud && \

update_app calendar $CALENDAR_VERSION && \
update_app circles $CIRCLES_VERSION && \
update_app contacts $CONTACTS_VERSION && \
update_app files_accesscontrol $FILES_ACCESSCONTROL_VERSION && \
update_app files_automatedtagging $FILES_AUTOMATEDTAGGING_VERSION && \
update_app files_retention $FILES_RETENTION_VERSION && \
update_app groupfolders $GROUPFOLDERS_VERSION && \
update_app ojsxc $OJSXC_VERSION && \
update_app richdocuments $RICHDOCUMENTS_VERSION && \
update_app spreed $SPREED_VERSION && \
update_app tasks $TASKS_VERSION && \
update_app twofactor_totp $TWOFACTOR_TOTP_VERSION && \

exit 0
