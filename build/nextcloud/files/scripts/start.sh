#!/bin/bash

# start cron
/etc/init.d/cron start

# install/upgrade nextcloud if not already current
/usr/local/sbin/install.sh && \

# # ensure all supported 3rd party apps are current
# /usr/local/sbin/update_apps.sh && \

# start nginx
/etc/init.d/php7.0-fpm start && \
/usr/sbin/nginx -g 'daemon off;'
