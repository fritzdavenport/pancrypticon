#!/usr/bin/python

import json, requests, re, subprocess, sys, time
from operator import itemgetter
from os import path, environ

apps = [
    'calendar',
    'circles',
    'contacts',
    'files_accesscontrol',
    'files_automatedtagging',
    'files_retention',
    'groupfolders',
    'ojsxc',
    'richdocuments',
    'spreed',
    'tasks',
    'twofactor_totp',
]

r = requests.get('https://apps.nextcloud.com/api/v1/platform/%s/apps.json' \
    % environ['NEXTCLOUD_VERSION'])

if r.status_code == requests.codes.ok:
    applist = r.json()

    for app in applist:
        app_path = '/opt/nextcloud/apps/%s' % app['id']
        if not path.isdir(app_path):
            if app['id'] in apps:
                releases = sorted(app['releases'], key=itemgetter('created'))
                if len(releases):
                    latest = releases[-1]
                    print "App '%s' not found: installing version %s ..." % \
                        (app['id'], latest['version'])
                    data = requests.get(latest['download'])
                    filename = '%s-%s.tar.gz' % (app['id'], latest['version'])
                    with open(filename, 'wb') as f:
                        f.write(data.content)
                    subprocess.call(['/bin/tar', '-C', '/opt/nextcloud/apps', \
                        '-xpf', filename])
                    sys.stdout.flush()
                    subprocess.call(['/bin/chown', '-R', 'www-data:www-data', \
                        app_path])
                    sys.stdout.flush()
                    subprocess.call(['/usr/bin/sudo', '-u', 'www-data', 'php', \
                        'occ', 'app:enable', app['id']])
