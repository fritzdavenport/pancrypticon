<?php
$CONFIG = array (
    'trusted_domains' =>
        array (
            0 => 'NEXTCLOUD_DOMAIN_NAME',
            1 => 'HIDDEN_DOMAIN_NAME',
        ),
    'datadirectory' => '/opt/nextcloud-data',
    'skeletondirectory' => '/opt/nextcloud-data/skeleton_files',
    'enable_previews' => true,
    'token_auth_enforced' => true,
    'filelocking.enabled' => true,
    'updatechecker' => false,
    'upgrade.disable-web' => true,
    'memcache.local' => '\OC\Memcache\Redis',
    'memcache.locking' => '\OC\Memcache\Redis',
    'redis' => array(
        'host' => 'redis',
        'port' => 6379,
    ),
    'enabledPreviewProviders' => array(
        'OC\Preview\Image',
    ),
    'htaccess.RewriteBase' => '/',
    'mail_smtpmode' => 'smtp',
    'mail_smtpauth' => false,
    'mail_from_address' => 'no-reply',
    'mail_domain' => 'NEXTCLOUD_DOMAIN_NAME',
    'mail_smtphost' => 'postfix',
    'mail_smtpport' => '25',
    'theme' => 'NEXTCLOUD_THEME',
    'default_language' => 'LANGUAGE',
);
