#!/bin/sh

EXTERNAL_IP=`/usr/bin/curl -s http://checkip.dyndns.org | \
    /usr/bin/awk '{ print $6 }' | /usr/bin/awk -F '<' '{ print $1 }'`

/bin/sed -i "s|TURN_SECRET|$TURN_SECRET|g" /etc/turnserver.conf
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/turnserver.conf
/bin/sed -i "s|EXTERNAL_IP|$EXTERNAL_IP|g" /etc/turnserver.conf

/usr/bin/turnserver -c /etc/turnserver.conf -v
