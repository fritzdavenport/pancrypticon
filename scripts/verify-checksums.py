#!/usr/bin/python

import json, os, re
from hashlib import sha256

def checksum(filename):
    '''
    returns a sha256 hash for a given file
    '''
    hash_sha256 = sha256()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256.hexdigest()

# Abort if the checksum file sig is bad
if os.system('gpg --verify checksums.json.asc checksums.json'):
    raise RuntimeError("FATAL: checksums.json has an invalid GPG signature!")

# load and parse the checksum.json file
reference = {}
with open('checksums.json', 'r') as infile:
    reference = json.load(infile)

for dirpath, dirnames, filenames in os.walk('.'):
    for f in sorted(filenames):
        # strip the leading './' from the file paths'
        filename = re.sub('^\.\/', '', os.path.join(dirpath, f))
        # skip files we know should not be checked
        if re.match('\.git\/', filename) or \
             re.match('checksums\.json(\.asc)?', filename) or \
               re.match('settings.env$', filename):
            pass
        # warn on any other files not being tracked
        elif filename not in reference:
            print("WARNING: %s - no checksum found for comparison" % filename)
        else:
            # compare file's checksum to the reference. Abort if there's a mismatch
            chksum = checksum(filename)
            if reference[filename] != chksum:
                print("FATAL: %s - %s does not equal %s" % (filename, chksum, reference[filename]))
                raise RuntimeError("Checksum mismatch")
