#!/bin/bash

cd /opt/pancrypticon/pancrypticon && \

# check for updates
if [ `git pull -v --dry-run 2>&1 | \
        grep 'origin/master' | \
        grep -c 'up to date'` != '1' \
];
then
    git pull && sudo scripts/verify-checksums.py || exit $?
    docker-compose pull
    docker-compose up -d --build --remove-orphans
fi

# remove old containers, images, etc
/usr/bin/docker system prune --all --force
